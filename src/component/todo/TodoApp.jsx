import React,{ Component } from 'react';
import {BrowserRouter as Router,Route,Switch,Link} from 'react-router-dom'
import AuthenticationService from './AuthenticationService.js'
export default class TodoApp extends Component
{
    render()
    {
        return(
            <div className="TodoApp">    
                <Router>
                    <HeaderComponent/>
                    <Switch>
                        <Route path="/" exact component={Login}/> 
                        <Route path="/login" component={Login}/>
                        <Route path="/logout" component={LogoutComponent}/>
                        <Route path="/welcome/:name" component={Welcome}/>
                        <Route path="/listtodos" component={ListTodosComponent}/>
                        <Route component={ErrorComponent}/>
                    </Switch>
                    <FooterComponent/>
                </Router>
                    {/* <Welcome/>
                <Login/> */}
            </div>
        );
    }
}
class HeaderComponent extends Component{
    render(){
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn;
        console.log(isUserLoggedIn);
    return(
        <header>
            <nav className="navbar navbar-expand-md bg-dark navbar-dark">
                <div><a href="https://www.geeksforgeeks.org/">GeeksForGeeks</a></div>
                <ul className="navbar-nav">
                    <li><Link className="nav-link" to="/welcome/tarun">Home</Link></li>
                    <li><Link className="nav-link" to="/listtodos">Todos</Link></li>
                </ul>
                <ul className="navbar-nav navbar-collapse justify-content-end">
                    <li><Link className="nav-link" to="/login">Login</Link></li>
                    <li><Link className="nav-link" to="/logout">Logout</Link></li>
                </ul>
            </nav>
        </header>
    )
    }
}
class FooterComponent extends Component{
    render()
    {
    return(
       <footer className="footer">
            <span className="text-muted">All Rights Reserved 2018 @SmartCorporation</span>
       </footer>
    )
    }
}
class LogoutComponent extends Component
{
 render(){
     return(
         <>
         <h1>You are logged out</h1>
         <div className="container">
         Thank you for Using Our Application.
         </div>
         </>
     )
 }
}
class ErrorComponent extends Component
{
    render(){

    return(
        <div>An Error Occured, Please Contact here at tarun.p.consultadd@gmail.com</div>
    )
    }
}


// function ShowLoginSuccessMessage(props)
// {
//     if(props.showSuccessMessage)
//         return <div>Login Succeed</div>
//     return null
// }
//  function ShowInvalidCredentials(props)
// {
//     if(props.hasLoginFailed)
//         return <div>Invalid Credentials</div>
//     return null
// }


export class Login extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            username:"",
            password:"",
            hasLoginFailed : false,
            showSuccessMessage:false
        }
        //this.handleUserNameChange = this.handleUserNameChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);
    }
    loginClicked()
    {
        if(this.state.username==="tarun" && this.state.password==="Tarun@123")
        {
            AuthenticationService.registerSuccessfulLogin(this.state.username,this.state.password);
            this.props.history.push(`/welcome/${this.state.username}`)
            // this.setState({
            //     showSuccessMessage:true
            //         })
            //  this.setState({hasLoginFailed:false})       
        }
        else{
            this.setState({
                showSuccessMessage:false
            })
            this.setState({ hasLoginFailed:true})
        }
    }
    handleChange(event)
    {
        this.setState({[event.target.name]: event.target.value});
    }
    render()
    {
        return(
            <div>
                <h1>Login</h1>
                <div className = "container">
                {this.state.showSuccessMessage && <div>Login Succeed</div>}
                {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                {/* <ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed}/>
                <ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage}/> */}
                UserName : <input type="text" name="username" value={this.state.username} onChange={this.handleChange}/>
                Password : <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/>
                <button onClick={this.loginClicked }>Login</button>
                </div>
            </div>
            );
    }
}


export class ListTodosComponent extends Component{
    constructor(props)
    {
        super(props)
        this.state={
            todo : [
                        {id:1 , description : 'Learn React from Scratch.....', done :false, targetDate: new Date()},
                        {id:2 , description : 'Learn Python from Scratch.....', done :false, targetDate: new Date()},
                        {id:3 , description : 'Learn Java from Scratch.....', done :false, targetDate: new Date()},
                        {id:4 , description : 'Learn Nodejs from Scratch.....', done :false, targetDate: new Date()},
                        {id:5 , description : 'Learn Angular from Scratch.....', done :false, targetDate: new Date()}
                    ]
        }
    }
    render(){
        return(
            <div>
                <h1>List Todos</h1>
                <div className = "container">
                <table className="table">
                    <thead>
                       <tr>
                           <th>Description</th>
                           <th>Target Date</th>
                           <th>Is Completed ?</th>
                       </tr>
                    </thead>
                    <tbody>
                            {this.state.todo.map(
                                todo =>
                                <tr>
                                    <td>{todo.description}</td>
                                    <td>{todo.done.toString()}</td>
                                    <td>{todo.targetDate.toString()}</td>
                                </tr>
                             )
                            } 
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

export class Welcome extends Component{
    render(){
        return(
            <>
            <h1>Welcome!</h1>
            <div className="container">
                Welcome to the Hell...... {this.props.match.params.name} <br/> you can manage you Todos here....... <Link to="/listtodos">Here</Link>
            </div>
            </>
        );
    }
}